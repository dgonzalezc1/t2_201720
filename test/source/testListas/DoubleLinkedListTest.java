package testListas;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class DoubleLinkedListTest<E> extends TestCase
{

	private DoubleLinkedList lista;
	

	
	public void setup1()
	{
		lista = new DoubleLinkedList<E>();

	}
	
	public void setup2()
	{
		lista = new DoubleLinkedList<E>();
		Object cosa1 = 1;
		lista.addFirst(cosa1);
		
	}
	
	public void setup3()
	{
		lista = new DoubleLinkedList<E>();
		Object cosa1 = 1;
		Object cosa2 = 2;
		Object cosa3 = 3;
		Object cosa4 = 4;
		Object cosa5 = 5;
		Object cosa6 = 6;
		lista.addLast(cosa1);
		lista.addLast(cosa2);
		lista.addLast(cosa3);
		lista.addLast(cosa4);
		lista.addLast(cosa5);
		lista.addLast(cosa6);
		
	}
	
	public void testSize()
	{
		setup1();
		assertEquals(0, lista.size());
		
	}
	
	public void testSize1()
	{
		setup2();
		assertEquals(1, lista.size());
	}
	public void testSize2()
	{
		setup3();
		assertEquals(6, lista.size());
	}
	
	public void testGetNode()
	{
		setup3();
		assertEquals(4, lista.getNode(4).getElement());
		assertEquals(3, lista.getNode(3).getElement());
		assertEquals(1, lista.getNode(1).getElement());
	}
	
	public void testAddPos()
	{
		setup3();
		Object cosa7 = 2;
		Object cosa8 = 8;
		lista.addPos(cosa7, 2);
		assertEquals(2, lista.getNode(2).getElement());
		assertEquals(2, lista.getNode(3).getElement());
		assertEquals(3, lista.getNode(4).getElement());
		assertEquals(6, lista.getNode(7).getElement());
		lista.addPos(cosa8, 7);
		assertEquals(6, lista.getNode(8).getElement());
		assertEquals(8, lista.size());

	}
	
	public void testRemovePos()
	{
		setup3();
		lista.removePos(1);
		assertEquals(2, lista.getNode(1).getElement());
		lista.removePos(5);
		assertEquals(5, lista.getNode(4).getElement());
		lista.removePos(2);
		assertEquals(4, lista.getNode(2).getElement());


	}
}
