package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */


public interface IList<E> extends Iterable<E> {


	Integer getSize();
	
	public void add(E cosa);
	public void addAtEnd(E cosa);
	public void addAtK(E cosa, int pos);
	public E getCurrentElement();
	public E getElement(int pos);
	public void delete();
	public void deleteAtK(int pos);
	public void next();
	public void previous();

}
