package model.vo;

public class VOTrip 
{
	private String route_id;
	private String service_id;
	private String trip_id;
	private String trip_head_sign;
	private String trip_short_name;
	private String direction_id;
	private String block_id;
	private String shape_id;
	private String wheelchair_accessible;
	private String bikes_allowed;
	
	
	public VOTrip(String pRoute_id, String pService_id, String pTrip_id, String pTrip_head_sign, String pTrip_short_name, String pDirection_id, String pBlock_id, String pShape_id, String pWheelchair_accessible, String pBikes_allowed  )
	{
		route_id = pRoute_id;
		service_id = pService_id;
		trip_id = pTrip_id;
		trip_head_sign = pTrip_head_sign;
		trip_short_name = pTrip_short_name;
		direction_id = pDirection_id;
		block_id = pBlock_id;
		shape_id = pShape_id;
		wheelchair_accessible = pWheelchair_accessible;
		bikes_allowed = pBikes_allowed;
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String route_id() 
	{
		return route_id;
	}
	
	public String service_id() 
	{
		return service_id;
	}
	
	public String trip_id() 
	{
		return trip_id;
	}
	
	public String trip_head_sign() 
	{
		return trip_head_sign;
	}
	
	public String trip_short_name() 
	{
		return trip_short_name;
	}
	
	public String direction_id() 
	{
		return direction_id;
	}
	
	public String block_id() 
	{
		return block_id;
	}
	
	public String shape_id() 
	{
		return shape_id;
	}
	
	public String wheelchair_accessible() 
	{
		return wheelchair_accessible;
	}
	
	public String bikes_allowed() 
	{
		return bikes_allowed;
	}

}
