package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {


	private String stop_id;
	private String stop_codigo;
	private String stop_nombre;
	private String stop_desc;
	private String stop_lat;
	private String stop_lon;
	private String zone_id;
	private String stop_url;
	private String location_type;
	private String parent_station;
	
	
	public VOStop(String pStop_id, String pStop_codigo, String pStop_nombre, String pStop_desc, String pStop_lat, String pStop_lon, String pZone_id, String pStop_url, String pLocation_type, String pParent_station  )
	{
		stop_id = pStop_id;
		stop_codigo = pStop_codigo;
		stop_nombre = pStop_nombre;
		stop_desc = pStop_desc;
		stop_lat = pStop_lat;
		stop_lon = pStop_lon;
		zone_id = pZone_id;
		stop_url = pStop_url;
		location_type = pLocation_type;
		parent_station = pParent_station;
	}

	
	/**
	 * @return id - Route's id number
	 */
	public String id() 
	{
		return stop_id;
	}
	
	public String codigo() 
	{
		return stop_codigo;
	}
	
	public String nombre() 
	{
		return stop_nombre;
	}
	
	public String desc() 
	{
		return stop_desc;
	}
	
	public String lat() 
	{
		return stop_lat;
	}
	
	public String lon() 
	{
		return stop_lon;
	}
	
	public String zone_id() 
	{
		return zone_id;
	}
	
	public String url() 
	{
		return stop_url;
	}
	
	public String location_type() 
	{
		return location_type;
	}
	
	public String parent_station() 
	{
		return parent_station;
	}

}
