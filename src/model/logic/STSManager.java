package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import model.vo.VOZone;
import model.data_structures.DoubleLinkedList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager 
{

	private RingList<VORoute> listaRutas;

	private DoubleLinkedList<VOStopTimes> listaStopTimes;

	private RingList<VOTrip> listaViajes;

	private RingList<VOStop> listaParadas;

	private RingList<VOZone> listaZonas;

	@Override
	public void loadRoutes(String routesFile) 
	{
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(routesFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VORoute ruta = new VORoute (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaRutas.addLast(ruta);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	@Override
	public void loadTrips(String tripsFile) 
	{
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTrip viaje = new VOTrip (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				listaViajes.addLast(viaje);

				linea = br.readLine();
			}
			br.close();


			for (int i = 0; i < listaRutas.size(); i++) 
			{
				for (int j = 0; j < listaViajes.size(); j++) 
				{
					if( ((VOTrip) listaViajes.getElement(j)).route_id().equals(((VORoute) listaRutas.getElement(i)).id()))
					{
						((VORoute) listaRutas.getElement(i)).agregarViaje( (VOTrip) listaViajes.getElement(j) );
					}
				}
			}

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	@Override
	public void loadStopTimes(String stopTimesFile) 
	{

		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStopTimes stopTime = new VOStopTimes (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8]);
				listaStopTimes.addLast(stopTime);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	@Override
	public void loadStops(String stopsFile) 
	{

		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStop parada = new VOStop (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				listaParadas.addLast(parada);

				linea = br.readLine();
			}

			br.close();

			for (int i = 0; i < listaParadas.size(); i++) 
			{
				boolean sePuedeAgregar = false;

				for (int j = 0; j < listaZonas.size(); j++) 
				{
					if ( !(((VOStop) listaParadas.getElement(i)).zone_id().equals(((VOZone) listaZonas.getElement(j)).zone_id())))
					{
						sePuedeAgregar = true;
					}

					else
					{
						((VOZone) listaZonas.getElement(j)).agregarParada(((VOStop) listaParadas.getElement(i)));
					}
				}

				if (sePuedeAgregar)
				{
					VOZone zona = new VOZone ( ((VOStop) listaParadas.getElement(i)).zone_id() , (VOStop) listaParadas.getElement(i));
					listaZonas.addFirst(zona);
				}

			}

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}

	@Override
	public DoubleLinkedList<VORoute> routeAtStop(String stopName) 
	{
		String stop_id = null;

		for (int i = 0; i < listaParadas.size(); i++) 
		{
			if (((VOStop) listaParadas.getElement(i)).nombre().equals(stopName))
			{
				stop_id = ((VOStop) listaParadas.getElement(i)).id();
			}
		}

		DoubleLinkedList<String> trips_id = new DoubleLinkedList<String>();

		for (int i = 0; i < listaStopTimes.size(); i++) 
		{
			if (((VOStopTimes) listaStopTimes.getElement(i)).stop_id().equals(stop_id))
			{
				trips_id.addFirst(((VOStopTimes) listaStopTimes.getElement(i)).trip_id());
			}
		}

		DoubleLinkedList<String> routes_id = new DoubleLinkedList<String>();

		for (int i = 0; i < listaViajes.size(); i++) 
		{
			for (int j = 0; j < trips_id.size(); j++) 
			{
				if(((VOTrip) listaViajes.getElement(i)).trip_id().equals(trips_id.getElement(j)))
				{
					routes_id.addFirst(((VOTrip) listaViajes.getElement(i)).route_id());
				}
			}
		}

		DoubleLinkedList<VORoute> rutas = new DoubleLinkedList<VORoute>() ;

		for (int i = 0; i < listaRutas.size(); i++) 
		{
			for (int j = 0; j < routes_id.size(); j++) 
			{
				if(((VORoute) listaRutas.getElement(i)).id().equals(routes_id.getElement(j)))
				{
					rutas.addFirst(((VORoute) listaRutas.getElement(i)));
				}
			}
		}

		return rutas;

	}

	@Override
	public DoubleLinkedList<VOStop> stopsRoute(String routeName, String direction) 
	{
		String route_id = null;

		for (int i = 0; i < listaRutas.size(); i++) 
		{
			if (((VORoute) listaRutas.getElement(i)).nombre_corto().equals(routeName))
			{
				route_id = ((VORoute) listaRutas.getElement(i)).id();
			}
		}


		DoubleLinkedList<VOTrip> trips_id = new DoubleLinkedList<VOTrip>();

		for (int i = 0; i < listaViajes.size(); i++) 
		{
			if (((VOTrip) listaViajes.getElement(i)).route_id().equals(route_id) && ((VOTrip) listaViajes.getElement(i)).direction_id().equals(direction))
			{
				trips_id.addFirst(((VOTrip) listaViajes.getElement(i)));
			}
		}

		DoubleLinkedList<String> stop_ids = new DoubleLinkedList<String>();

		for (int i = 0; i < listaStopTimes.size(); i++) 
		{
			for (int j = 0; j < trips_id.size(); j++) 
			{
				if (((VOStopTimes) listaStopTimes.getElement(i)).trip_id().equals(trips_id.getElement(j)))
				{
					stop_ids.addFirst(((VOStopTimes) listaStopTimes.getElement(j)).stop_id());
				}
			}
		}
		
		DoubleLinkedList<VOStop> stops = new DoubleLinkedList<VOStop>();
		
		for (int i = 0; i < listaParadas.size(); i++) 
		{
			for (int j = 0; j < stop_ids.size(); j++) 
			{
				if(((VORoute) listaParadas.getElement(i)).id().equals(stop_ids.getElement(j)))
				{
					stops.addFirst(((VOStop) listaRutas.getElement(i)));
				}
			}
		}


		return stops;
	}

}
