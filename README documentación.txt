DOCUMENTACI�N

1.
No realizamos la implementacion de los metodos declarados en la interface IList. Esto es porque creemos que la documentacion de los metodos que debiamos declarar en la interface era muy poco clara y ambigua. En ningun lugar se especificaba que debian hacer los metodos del IList. Sin embargo los metodos implementados en el RingList y el DoubleLinkedList cumplen con la mayoria de requerimentos para una lista.

2.
Los tests de las listas van de acuerdo a los especificados en el enunciado del taller y algunos que consideramos pertinentes para el funcionamiento de todo el programa.
3.
Debido a que se trata de archivos CSV sencillo, se leyeron los archivos siguiendo la gu�a:  http://chuwiki.chuidiang.org/index.php?title=Leer_fichero_CSV_con_Java

VORoute - Clase que representa una ruta
VOTrip - Clase que representa un viaje
VOStop - Clase que representa una parada
VOStopTime - Clase que representa un tiempo de parada
VOZone - Clase que representa una zona

Los m�todos loadRoutes(String routesFile) y loadStopTimes(String stopTimesFile) guardan la informaci�n de los archivos conservando su orden inicial, en una RingList y DoubleLinkedList respectivamente. El m�todo loadStops(String stopsFile) guarda originalmente la informaci�n de las paradas dada por el archivo en una RingList que conserva el mismo orden, despu�s agrega las diferentes zonas a la lista de zonas y le agrega a cada una sus paradas ordenadas alfanumericamente teniendo como par�metro de comparaci�n el id que se tom� como un String. El m�todo loadTrips(String tripsFile) originalmente la informaci�n de los viajes dados por el archivo en una RingList que conserva el mismo orden, despu�s agrega a cada ruta de la lista de rutas los viajes que le correspondan y los ordena alfanumericamente teniendo como par�metro de comparaci�n el id que se tom� como un String. 

�Cu�l es el orden de complejidad de buscar una parada espec�fica en esta estructura de datos? 
Buscar una parada especifica en el peor de los casos es de O(n) si ya se cuenta con el id, c�digo o nombre de la parada. 

�Cu�l ser�a el orden de complejidad si se utiliza una lista doblemente (sencillamente) encadenada?
Tiene la misma complejidad en el peor de los casos.


4. 
Para este m�todo se cambi� y en vez de retornar IList se retorna DoubleLinkedList ya que anteriormente se hab�a decidido no implementar la lista IList.

5. 
Para este m�todo se cambi� y en vez de retornar IList se retorna DoubleLinkedList ya que anteriormente se hab�a decidido no implementar la lista IList.